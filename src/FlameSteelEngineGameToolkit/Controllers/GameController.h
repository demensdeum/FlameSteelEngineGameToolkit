/*
 * Controller.h
 *
 *  Created on: Jul 27, 2016
 *      Author: demensdeum
 */

#ifndef FSEGTCONTROLLER_H_
#define FSEGTCONTROLLER_H_

namespace FlameSteelEngine
{
namespace GameToolkit
{
class MainGameController;
}
}

#include <FlameSteelEngineGameToolkit/Controllers/FSEGTObjectsContext.h>
#include <FlameSteelEngineGameToolkit/IO/Renderers/FSEGTRenderer.h>
#include <FlameSteelEngineGameToolkit/Data/FSEGTGameData.h>
#include <FlameSteelCore/Controller.h>
#include <memory>

using namespace std;
using namespace FlameSteelCore;
using namespace FlameSteelEngine::GameToolkit;

namespace FlameSteelEngine
{
namespace GameToolkit
{

class GameController: public Controller
{
public:
    GameController();
    virtual ~GameController();

    virtual void beforeStart();
    virtual void step();
    virtual void beforeStop();

    void setGameController(MainGameController *gameController);
    MainGameController* getGameController();

    shared_ptr<FSEGTGameData> getGameData();
    virtual void setGameData(shared_ptr<FSEGTGameData> gameData);

    void setIOSystem(shared_ptr<FSEGTIOSystem> ioSystem);

    MainGameController *mainGameController;

    virtual void receivedActionFromSender(int action, Object *sender);

    shared_ptr<FSEGTObjectsContext> objectsContext;

protected:
    shared_ptr<FSEGTIOSystem> ioSystem;

    shared_ptr<FSEGTRenderer> renderer;

    shared_ptr<FSEGTGameData> gameData;
    MainGameController* gameController;
};

}
}

#endif /* FSEGTCONTROLLER_H_ */
