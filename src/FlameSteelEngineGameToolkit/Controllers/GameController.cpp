/*
 * Controller.cpp
 *
 *  Created on: Jul 27, 2016
 *      Author: demensdeum
 */

#include "GameController.h"
#include <FlameSteelCore/Object.h>

#include <iostream>

using namespace std;

GameController::GameController()
{
    gameController = nullptr;
}

void GameController::beforeStart()
{

    if (ioSystem.get() == nullptr)
    {

        //cout << "Controller: cannot get ioSystem on controller switching. Is it even initialized? Exit";

        exit(1);

    }

    if (ioSystem->renderer.get() == nullptr)
    {

        //cout << "Controller: cannot get ioSystem *Renderer* on controller switching. Is it even initialized? Exit";

        exit(1);

    }

    renderer = ioSystem->renderer;
}

void GameController::step()
{

}

void GameController::beforeStop()
{

}

void GameController::receivedActionFromSender(int, Object *)
{

}

void GameController::setGameController(MainGameController* gameController)
{
    this->gameController = gameController;
}

MainGameController* GameController::getGameController()
{
    return this->gameController;
}

shared_ptr <FSEGTGameData> GameController::getGameData()
{
    return this->gameData;
}

void GameController::setGameData(shared_ptr<FSEGTGameData> gameData)
{
    this->gameData = gameData;
}

void GameController::setIOSystem(shared_ptr<FSEGTIOSystem> ioSystem)
{
    this->ioSystem = ioSystem;
    this->renderer = ioSystem->renderer;
}

GameController::~GameController()
{
    // TODO Auto-generated destructor stub
}
