#ifndef FLAMESTEELENGINEGAMETOOLKITSCRIPTCONTROLLER_H_
#define FLAMESTEELENGINEGAMETOOLKITSCRIPTCONTROLLER_H_

#include <string>

using namespace std;

namespace FlameSteelEngine {
namespace GameToolkit {

class ScriptController {

public:
    virtual ~ScriptController() = default;
    virtual void step() = 0;
    void setScript(string script);

protected:
    string script;

};

}
}

#endif