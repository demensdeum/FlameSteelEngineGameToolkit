/*
 * FSEGTRenderer.h
 *
 *  Created on: Aug 6, 2016
 *      Author: demensdeum
 */

#ifndef FSEGTRENDERER_H_
#define FSEGTRENDERER_H_

#include <FlameSteelCommonTraits/Screenshot.h>
#include <FlameSteelCommonTraits/IOSystemParams.h>
#include <FlameSteelEngineGameToolkit/Data/FSEGTGameData.h>
#include <FlameSteelEngineGameToolkit/IO/IOSystems/FSEGTIOSystem.h>
#include <FlameSteelCore/Object.h>

#include <memory>

using namespace std;
using namespace FlameSteelEngine::GameToolkit;

class FSEGTRenderer : public Object, public FSEGTObjectContextDelegate
{
public:
    FSEGTRenderer();

    virtual void preInitialize();
    virtual void fillParams(shared_ptr<IOSystemParams> params) = 0;
    virtual void initializeWindow(shared_ptr<IOSystemParams> params) = 0;

    virtual void render(shared_ptr<FSEGTGameData> gameData) = 0;

    virtual void beforeStop();

    virtual void blankScreen();

    virtual void updateScreen();

    shared_ptr<FSEGTIOSystem> ioSystem;

    virtual void setWindowTitle(string title) = 0;

    virtual ~FSEGTRenderer();

    virtual void objectsContextObjectAdded(shared_ptr<FSEGTObjectsContext> context, shared_ptr<Object> object) = 0;

    //virtual void addRenderID(int id) = 0;

    virtual void cleanRenderIDs();

    virtual shared_ptr<Screenshot> takeScreenshot() = 0;
};

#endif /* FSEGTRENDERER_H_ */
