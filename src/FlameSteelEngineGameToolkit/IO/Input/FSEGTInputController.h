/*
 * FSEGTInputController.h
 *
 *  Created on: Aug 7, 2016
 *      Author: demensdeum
 */

#ifndef FSEGTINPUTCONTROLLER_H_
#define FSEGTINPUTCONTROLLER_H_

#include <FlameSteelEngineGameToolkit/Data/Components/Touch/FSEGTTouch.h>
#include <FlameSteelCore/Objects.h>
#include <vector>
#include <memory>
#include <set>

using namespace std;
using namespace FlameSteelCore;

class FSEGTIOSystem;

class FSEGTInputController: public Object
{
public:
    FSEGTInputController();

	virtual bool isButtonPressed(string key);

	virtual bool isPointerPressed();
	virtual float pressedPointerParameter(string id);

    virtual bool isJumpKeyPressed();
    virtual bool isCrouchKeyPressed();

    virtual bool isLeftKeyPressed();
    virtual bool isRightKeyPressed();
    virtual bool isUpKeyPressed();
    virtual bool isDownKeyPressed();

    virtual bool isRotateLeftKeyPressed();
    virtual bool isRotateRightKeyPressed();

    virtual bool isUseKeyPressed();
    virtual bool isShootKeyPressed();
    virtual bool isExitKeyPressed();

    virtual void pollKey();
    virtual void clearKeys();

    virtual int getPointerXdiff();
    virtual int getPointerYdiff();

    shared_ptr<FSEGTIOSystem> ioSystem;
    shared_ptr<Objects> touches = make_shared<Objects>();

    virtual ~FSEGTInputController();

public:
    bool jumpKeyPressed = false;
    bool crouchKeyPressed = false;

    bool leftKeyPressed = false;
    bool rightKeyPressed = false;
    bool downKeyPressed = false;
    bool upKeyPressed = false;
    bool exitKeyPressed = false;

    bool useKeyPressed = false;
    bool shootKeyPressed = false;

    bool rotateLeftKeyPressed = false;
    bool rotateRightKeyPressed = false;

	bool pointerPressed = false;

    void addTouch(shared_ptr<FSEGTTouch> touch);
    void removeTouch(shared_ptr<FSEGTTouch> touch);

    void clearTouches();

    int pointerXdiff = 0;
    int pointerYdiff = 0;

    float wheelDiffY = 0;

	set<string> pressedButtonsSet;
	map<string, float> pressedPointerParametersMap;

};

#endif /* FSEGTINPUTCONTROLLER_H_ */
